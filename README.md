# Hackathon - Rafteurs

    Eloi Lassarat
    Maxime Barathon

# Documentation infra

## Site

https://hackaton-rafteur-node.osc-fr1.scalingo.io/

## 1. Introduction

Notre projet consiste à fournir une interface web permettant de se renseigner sur la disponibilité des stands de vélo dans la ville de Nantes.

![Structure du projet](src\assets\img\Rafteurs.png)

- **Open data**: https://data.nantesmetropole.fr/explore/dataset/244400404_stations-velos-libre-service-nantes-metropole-disponibilites/table/. Il se peut que ce service soit innaccessible de temps à autre.
- **Service provider et CD**: Scalingo. C'est le service sur lequel nous déployons notre projet/site. De plus scalingo implémente un système de CD au travers du fichier Procfile.
- **Gestionnaire de source**: GitLab. Nous utilisons gitlab par facilité d'utilisation et disponibilité d'accès à gitlab-ci. https://gitlab.imt-atlantique.fr/rafteurs/hackathon.git
- **CI**: gitlab-ci. La CI est paramétrée dans le fichier ".gitlab-ci.yml" et est accessible dans notre git via https://gitlab.imt-atlantique.fr/rafteurs/hackathon/-/pipelines
- **Futur backend**: python. Le projet a initialement été implémenté avec un backend python mais pour des raisons formatives, de facilité et de légèreté nous avons temporairement désactivé le backend python pour implémenter les appels à l'api data.nantesmetropole directement en typescript. Le backend python étant toujours disponible dans le projet mais inutilisé.

## 2. Développement local

Pour éditer le projet, cloner le projet git `git clone git@gitlab.imt-atlantique.fr:rafteurs/hackathon.git`
S'assurer que toutes les dépendances soient installées avec `npm install`
Vous pouvez lancer le projet localement par la commande `npm run dev`
Si vous souhaitez faire vos tests localement vous pouvez installer des dépendances de développement:

```bash
npm install -g @vue/cli
npm install --save-dev jest @vue/test-utils
npm install --save-dev ts-jest
```

Puis lancer `npm run test:unit`, cela va lancer les tests déclarés dans "tests/**tests**" (sous la forme \_\_\_.spec.ts).

## 3. Développement git et déploiement

Notre projet implémente une structure de **gitflow**, ainsi si vous souhaitez contribuer au projet il est frotement recommandé de se créer une branche sous le format **feature\_\_\_** dérivant de la branch develop, vous pourrez ainsi tester votre fonctionnalité avant déploiement au travers d'un push de merge avec la brache develop. Cela va entrainer une CI (gitlab-ci), qui va tester les tests implémentés dans le projet. **A noter qu'en raison d'une disponibilité limité de runners pour le lancement de la CI, il convient de nous prévenir pour que l'on lance notre propre runner.** Ainsi vous pourrez voir (dans gitlab dans deploy/pipelines) si certaines modifications seraient à apporter à votre feature pour vérifier les tests et ensuite être déployé sur la branche main. Si tous les tests sont passés vous pourrez emmettre une merge request entre la branche main et develop. Dans le cas d'une acceptation de la part des Owners/Mainteners un CD (scalingo) sera enclenché et le site sera mis à jour et disponible publiquement https://hackaton-rafteur-node.osc-fr1.scalingo.io/.

## 3. Backend et Frontend

**Frontend**: Notre projet est un projet Node/Vue3 en typescript ainsi si vous souhaitez accéder à la programmation de notre page d'accueil, vous pouvez vous rendre dans "src\views\OurPresentation\OurPresentationView.vue".

**Backend**: Le backend est une API REST coder avec Flask. Le code se trouve sur "api.py". Le code est fonctionnel mais pas déployé donc les changements ne seront pas visibles sur le site.
