// __tests__/api.test.ts
import { getNames } from "../../composable/useOpenData";

describe('getNames', () => {
  it('should return data with HTTP status 200', async () => {
    const result = await getNames();
    let found = false;
    result.forEach((item) => {
      if (item.name === '01011 - BORNE TEST NANTES 1') {
        found = true;
      }
    });
    expect(found).toBe(true);
  });
});