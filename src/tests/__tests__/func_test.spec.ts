// __tests__/example.spec.ts
import { add } from '../example';

describe('add function', () => {
  it('should add two numbers', () => {
    expect(add(2, 3)).toBe(5);
  });
});
