import { createSSRApp, createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router/index.js";

// Nucleo Icons
import "./assets/css/nucleo-icons.css";
import "./assets/css/nucleo-svg.css";

import materialKit from "./material-kit.js";

export function createOurApp() {
  const preApp = createSSRApp(App);
  preApp.use(createPinia());
  preApp.use(router);
  preApp.use(materialKit);
  console.log(preApp);
  return preApp;
}
