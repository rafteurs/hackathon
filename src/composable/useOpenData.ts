import axios from "axios";

const base_url_api = "https://data.nantesmetropole.fr";

export const getNames = async () => {
  const api_request =
    "/api/explore/v2.1/catalog/datasets/244400404_stations-velos-libre-service-nantes-metropole-disponibilites/records?limit=100";
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url);
    return response.data.results;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};
