import axios from "axios";
import router from "../router";

const base_url_api = "https://hackaton-rafteur-back.osc-fr1.scalingo.io/";
// const base_url_api = "http://127.0.0.1:5000/";

const dataHeaders: any = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

export const login = async (data_username, data_psw) => {
  const api_request = "login";
  const api_url = base_url_api + api_request;
  const data_input = { username: data_username, password: data_psw };
  try {
    const response = await axios.post(api_url, data_input, {
      headers: dataHeaders,
    });
    console.log(response.data);

    //Cookie association
    document.cookie = "session_id=" + response.data.session_id.toString();

    router.push({ name: "homeEdit" });
    return false;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
    if (error.message == "Request failed with status code 400") {
      return true;
    }
    return false;
  }
};

export const getInfosActivity = async (id, lib_activite, lieu_act_voie) => {
  const api_request = "activity";
  const api_url = base_url_api + api_request;
  const data_input = {
    id: id,
    lib_activite: lib_activite,
    lieu_act_voie: lieu_act_voie,
  };
  try {
    const response = await axios.post(api_url, data_input, {
      headers: dataHeaders,
    });
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getInfosActivityDB = async (id) => {
  const api_request = "activitydb/" + id;
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url, {
      headers: dataHeaders,
    });
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getInfosDB = async () => {
  const bd_request = "activitiesdb";
  const bd_url = base_url_api + bd_request;
  try {
    const bd_response = await axios.get(bd_url, { headers: dataHeaders });
    console.log(bd_response);
    const response = bd_response.data;
    return response;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getInfosHome = async () => {
  const api_request = "all";
  const api_url = base_url_api + api_request;
  try {
    const api_response = await axios.get(api_url, { headers: dataHeaders });
    console.log(api_response);
    const response = api_response.data;
    return response;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getFilters = async () => {
  const api_request = "filters";
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url, { headers: dataHeaders });
    return response.data;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getInfosHomeEdit = async () => {
  const api_request = "activitiesdb";
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url, { headers: dataHeaders });
    return response.data;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const delActivity = async (id) => {
  const api_request = "delactivitydb/" + id;
  const api_url = base_url_api + api_request;
  const co = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("session_id="));
  if (co) {
    const data_input = {
      session_id: co.split("=")[1],
    };
    try {
      const response = await axios.post(api_url, data_input, {
        headers: dataHeaders,
      });
      return response.data;
    } catch (error) {
      console.error("Erreur lors de l appel :", error);
    }
  }
};

export const getInfosFiltered = async (filter) => {
  const api_request = "allfiltered/" + filter;
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url, { headers: dataHeaders });
    return response.data;
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const getInfosActivitydb = async (id) => {
  const api_request = "activitydb/" + id.toString();
  const api_url = base_url_api + api_request;
  try {
    const response = await axios.get(api_url, { headers: dataHeaders });
    return [
      response.data.name,
      response.data.description,
      response.data.address,
      response.data.tel,
      response.data.mail,
    ];
  } catch (error) {
    console.error("Erreur lors de l appel :", error);
  }
};

export const createInfosActivitydb = async (name, address, desc, tel, mail) => {
  const api_request = "activitydb";
  const api_url = base_url_api + api_request;
  const co = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("session_id="));
  if (co) {
    const data_input = {
      session_id: co.split("=")[1],
      name: name,
      address: address,
      description: desc,
      website: "",
      theme: "BDX",
      tel: tel,
      mail: mail,
    };
    try {
      const response = await axios.post(api_url, data_input, {
        withCredentials: true,
        headers: dataHeaders,
      });
      return [
        response.data.name,
        response.data.description,
        response.data.address,
        response.data.tel,
        response.data.mail,
      ];
    } catch (error) {
      console.error("Erreur lors de l appel :", error);
    }
  }
};

export const updateInfosActivitydb = async (
  id,
  name,
  address,
  desc,
  tel,
  mail
) => {
  const api_request = "activitydb/" + id.toString();
  const api_url = base_url_api + api_request;
  const co = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("session_id="));
  if (co) {
    const data_input = {
      session_id: co.split("=")[1],
      name: name,
      address: address,
      description: desc,
      website: "",
      theme: "BDX",
      tel: tel,
      mail: mail,
    };
    try {
      const response = await axios.put(api_url, data_input, {
        withCredentials: true,
        headers: dataHeaders,
      });
      return [
        response.data.name,
        response.data.description,
        response.data.address,
        response.data.tel,
        response.data.mail,
      ];
    } catch (error) {
      console.error("Erreur lors de l appel :", error);
    }
  }
};
