import { createOurApp } from "./main";

const app = createOurApp();

app.mount("#app");
