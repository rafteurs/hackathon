import { createRouter, createWebHistory } from "vue-router";
import OurPresentationView from "../views/OurPresentation/OurPresentationView.vue";
import ActivityPage from "../views/ActivityPage/ActivityPage.vue"
import ActivityPageEdit from "../views/ActivityPage/ActivityPageEdit.vue"
import HomePageView from "../views/HomePage/HomePage.vue";
import HomePageViewEdit from "../views/HomePage/HomePageEdit.vue";
import LoginPageView from "../views/Login/LoginPage.vue";
//import PresentationView from "../views/Presentation/PresentationView.vue";
// import AboutView from "../views/LandingPages/AboutUs/AboutView.vue";
// import ContactView from "../views/LandingPages/ContactUs/ContactView.vue";
// import AuthorView from "../views/LandingPages/Author/AuthorView.vue";
// import SignInBasicView from "../views/LandingPages/SignIn/BasicView.vue";
// import PageHeaders from "../layouts/sections/page-sections/page-headers/HeadersView.vue";
// import PageFeatures from "../layouts/sections/page-sections/features/FeaturesView.vue";
// import NavigationNavbars from "../layouts/sections/navigation/navbars/NavbarsView.vue";
// import NavigationNavTabs from "../layouts/sections/navigation/nav-tabs/NavTabsView.vue";
// import NavigationPagination from "../layouts/sections/navigation/pagination/PaginationView.vue";
// import InputAreasInputs from "../layouts/sections/input-areas/inputs/InputsView.vue";
// import InputAreasForms from "../layouts/sections/input-areas/forms/FormsView.vue";
// import ACAlerts from "../layouts/sections/attention-catchers/alerts/AlertsView.vue";
// import ACModals from "../layouts/sections/attention-catchers/modals/ModalsView.vue";
// import ACTooltipsPopovers from "../layouts/sections/attention-catchers/tooltips-popovers/TooltipsPopoversView.vue";
// import ElAvatars from "../layouts/sections/elements/avatars/AvatarsView.vue";
// import ElBadges from "../layouts/sections/elements/badges/BadgesView.vue";
// import ElBreadcrumbs from "../layouts/sections/elements/breadcrumbs/BreadcrumbsView.vue";
// import ElButtons from "../layouts/sections/elements/buttons/ButtonsView.vue";
// import ElButtonGroups from "../layouts/sections/elements/button-groups/ButtonGroupsView.vue";
// import ElDropdowns from "../layouts/sections/elements/dropdowns/DropdownsView.vue";
// import ElProgressBars from "../layouts/sections/elements/progress-bars/ProgressBarsView.vue";
// import ElToggles from "../layouts/sections/elements/toggles/TogglesView.vue";
// import ElTypography from "../layouts/sections/elements/typography/TypographyView.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/activity",
      name: "activity",
      component: ActivityPage,
    },
    {
      path: "/home",
      name: "home",
      component: HomePageView,
    },
    {
      path: "/homeEdit",
      name: "homeEdit",
      component: HomePageViewEdit,
    },
    {
      path: "/login",
      name: "login",
      component: LoginPageView,
    },
    {
      path: "/activityEdit",
      name: "activityEdit",
      component: ActivityPageEdit,
    }
  ]
});

export default router;
