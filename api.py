from flask import Flask, render_template, request, jsonify, make_response
import requests, os
from werkzeug.exceptions import NotFound

app = Flask(__name__)

host = os.getenv("HOST", "0.0.0.0")
proto = os.getenv("PROTO", "http")
port = os.environ.get("PORT", "5000")
url = proto + "://" + host + ":" + port
print(url)

base_url_api = "https://data.nantesmetropole.fr"


@app.route("/", methods=["GET"])
def home():
    api_request = "/api/explore/v2.1/catalog/datasets/244400404_stations-velos-libre-service-nantes-metropole-disponibilites/records?limit=20"
    response = requests.get(base_url_api + api_request)
    if response.status_code == 200:
        return f"<h1 style='color:blue'>{response.text}</h1>"
    else:
        return f"<h1 style='color:blue'>Connection failed with API</h1>"


@app.route("/names", methods=["GET"])
def get_names():
    api_request = "/api/explore/v2.1/catalog/datasets/244400404_stations-velos-libre-service-nantes-metropole-disponibilites/records?select=name&limit=20"
    response = requests.get(base_url_api + api_request)
    if response.status_code == 200:
        print(response.json()["results"])
        return {"names": response.json()["results"]}
    else:
        return {"error": "A problem occured while getting all the names"}


if __name__ == "__main__":
    app.run(host=host, port=port)
